===============================================

# Table contents
[TOC]

## Overview
This is a java based stand-alone software tool. The input files are two CMAP files and one XMAP file.

## Software design and implementation
#### Basic idea and algorithms

```
step 1: calculate positions of scaffolds
 define the coordinate of the first reference scaffold as (0,0)
 calculate the coordinate of corresponding query scaffolds
 continue to do the step 1 until all the locations of reference and query scaffolds.

step 2: 
  After step 1, the max width and height of graph can be known.
  Add padding space and centralize the graph in the middle of canvas

step 3:
  Draw reference and query scaffolds

step 4:
  Draw matching lines between reference and query scaffolds.

``` 
#### Technologies, languages
JDK 8 above
language: java

## How to install 
#### Input data file format
CMAP file: please reference demo file /home/linden/repo/scaffolder/r.cmap
XMAP file: please reference demo file /home/linden/repo/scaffolder/flax_scaffolded_bng_data_extended_0909.xmap

#### How to run the program
open the project from NeatBeans, and click 'run' button.