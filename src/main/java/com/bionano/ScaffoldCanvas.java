/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bionano;

import static com.bionano.ScaffoldConfig.*;
import static com.bionano.Tool.*;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import org.apache.batik.dom.GenericDOMImplementation;
import org.apache.batik.svggen.SVGGraphics2D;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;

/**
 * @author quanx
 */
public class ScaffoldCanvas extends JPanel implements MouseListener, ActionListener, MouseMotionListener {

    Graphics2D graph;
    LinkedHashMap<Integer, HashMap<Integer, Cmap.Item>> rList, qList;
    ScaffoldPositionCalculator pc;
    ArrayList<Xmap.Item> xList;
    Xmap xMap;
    Cmap rMap, qMap;
    int markHeight;
    final JPopupMenu popupMenu = new JPopupMenu();
    final JMenuItem menuSaveAsSVG = new JMenuItem("Save as SVG");
    final JMenuItem menuSaveAsPNG = new JMenuItem("Save as PNG");
    private Point origin;
    private Color rColor = GREENISH, qColor = BLUEISH;
    private int canvasWidth, canvasHeight;
    double minimumUnitLengh = 0.1; // unit is million 
    double minimumUnitNaturalLengh = 17; // unit is px
    boolean drawHitLines = false;

    private final HashMap<Integer, Boolean> entryDisplayMap = new HashMap();
    private ArrayList<Integer> currentRefIDs;

    private BufferedImage scissors, flipedScisseors;

    public ScaffoldCanvas() {
        initComponent();
        try {
            scissors = ImageIO.read(ScaffoldCanvas.class.getResource("/img/scissors.png"));
            scissors = createScaled(scissors, SCISSOR_SIZE_W, SCISSOR_SIZE_H);
            flipedScisseors = createFlipped(scissors);
        } catch (IOException ex) {
            Logger.getLogger(ScaffoldCanvas.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void smoothText() {
        graph.setRenderingHint(
                RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
    }

    private void smoothLine() {
        if (smoothLine) {
            graph.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        }
    }

    public void setMaps(Cmap rCmap, Cmap qCmap, Xmap xmap) {
        this.rMap = rCmap;
        this.qMap = qCmap;
        this.xMap = xmap;
        initEntryDisplayMap();
    }

    public int refresh(int referenceID) {
        return refresh(referenceID, rColor, qColor);
    }

    public int refresh(int referenceID, Color rColor, Color qColor) {
        ArrayList<Integer> list = new ArrayList();
        list.add(referenceID);
        return initData(list, rColor, qColor);
    }

    public int refreshByGroupID(String groupID) {
        return initDataByGroupID(groupID);
    }

    private void paint() {
        drawRefQue();
        drawMatch();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (graph != null) {
            graph.dispose();
        }

        graph = (Graphics2D) g;

        graph.setColor(Color.WHITE);
        // draw white background
        graph.fillRect(0, 0, getWidth(), getHeight());

        if (rMap == null || qMap == null || xMap == null) {
            return;
        }

        paint();
    }

    private void drawRefQue() {
        HashMap refList = new HashMap();
        HashMap queList = new HashMap();

        int size = xList.size();

        for (int i = 0; i < size; i++) {
            Xmap.Item xItem = xList.get(i);
            int qID = xItem.qID;
            int rID = xItem.rID;
            if (qID >=0 && queList.get(qID) == null) {
                drawQuery(xItem, pc.getQuePosition(qID));
                queList.put(qID, true);
            }
            if (refList.get(rID) == null) {
                drawReference(xItem, pc.getRefPosition(rID));
                refList.put(rID, true);
            }
        }
    }

    private void drawMatch() {
        int size = xList.size();
        for (int i = 0; i < size; i++) {
            Xmap.Item item = xList.get(i);
            int rID = item.rID;
            int qID = item.qID;
            
            if (qID<0)continue;
            
            boolean rOrientation = item.SSC_orientation.equals(PLUS);
            boolean qOrientation = item.SSC_scaffold_orientation.equals(PLUS);

            HashMap rItem = rMap.get(rID);
            HashMap qItem = qMap.get(qID);

            int[] rPosition = pc.getRefPosition(rID);
            int rXPosition = rPosition[ScaffoldPositionCalculator.X_COORDINATE];
            int rYPosition = rPosition[ScaffoldPositionCalculator.Y_COORDINATE];

            int[] qPosition = pc.getQuePosition(qID);
            int qXPosition = qPosition[ScaffoldPositionCalculator.X_COORDINATE];
            int qYPosition = qPosition[ScaffoldPositionCalculator.Y_COORDINATE];

            drawAllHitLines(true, rOrientation, rItem, rXPosition, rYPosition, item.rLen);
            drawAllHitLines(false, qOrientation, qItem, qXPosition, qYPosition, item.qLen);

            // draw Query start and end line
            drawHitLine(false, qOrientation, item.qStartPos, qXPosition, qYPosition, item.qLen, START_LINE_COLOR, StartEndLineWidth);
            drawHitLine(false, qOrientation, item.qEndPos, qXPosition, qYPosition, item.qLen, END_LINE_COLOR, StartEndLineWidth);

            // draw referebce start and end line
            drawHitLine(true, rOrientation, item.rStartPos, rXPosition, rYPosition, item.rLen, START_LINE_COLOR, StartEndLineWidth);
            drawHitLine(true, rOrientation, item.rEndPos, rXPosition, rYPosition, item.rLen, END_LINE_COLOR, StartEndLineWidth);

            drawConnection(rItem, qItem, rXPosition, rYPosition, qXPosition, qYPosition, rOrientation, qOrientation, item.alignment);

            drawShadow(rOrientation, qOrientation, item.rStartPos, item.rEndPos, item.qStartPos, item.qEndPos, rXPosition, rYPosition, qXPosition, qYPosition, item.rLen, item.qLen);
        }
    }

    private void drawShadow(boolean rOrientation, boolean qOrientation, double rStartPos, double rEndPos, double qStartPos, double qEndPos, int rXPos, int rYPos, int qXPos, int qYPos, double rLen, double qLen) {
        if (drawHitLines == false && matches) {
            //   p1      p2            // p1 always ref start point
            //    ---------            // p2 always ref end point
            //    |       |            // p3 always query end point
            //    ---------            // p4 always query start point
            //   p4        p3
            smoothLine();
            int p1x, p1y, p2x, p2y, p3x, p3y, p4x, p4y;

            int rSP = getRealLength(rStartPos);
            int rEP = getRealLength(rEndPos);

            int qSP = getRealLength(qStartPos);
            int qEP = getRealLength(qEndPos);

            int rTotalLength = getRealLength(rLen);
            int qTotalLength = getRealLength(qLen);

            if (rOrientation) {
                p1x = rXPos + rSP;
                p2x = rXPos + rEP;
            } else {
                p1x = rXPos + rTotalLength - rSP;
                p2x = rXPos + rTotalLength - rEP;
            }

            p1y = rYPos + markHeight + barNaturalHeight;
            p2y = p1y;

            if (qOrientation) {
                p3x = qXPos + qEP;
                p4x = qXPos + qSP;
            } else {
                p3x = qXPos + qTotalLength - qEP;
                p4x = qXPos + qTotalLength - qSP;
            }

            p3y = qYPos;
            p4y = qYPos;

            Polygon polygon = new Polygon();
            polygon.addPoint(p1x, p1y);
            polygon.addPoint(p2x, p2y);
            polygon.addPoint(p3x, p3y);
            polygon.addPoint(p4x, p4y);
            graph.setColor(hitColor);
            graph.fillPolygon(polygon);
        }
    }

    private int initDataByGroupID(String gid) {
        ArrayList<Integer> refIDs = xMap.getRefIDsByGID(gid);
        return initData(refIDs);
    }

    private int initData(ArrayList<Integer> refIDs) {
        return initData(refIDs, rColor, qColor);
    }

    // invoke setWindowSize method first to calculate locations.
    private int initData(ArrayList<Integer> refIDs, Color rColor, Color qColor) {
        currentRefIDs = refIDs;

        if (rMap == null || qMap == null || xMap == null) {
            return CANVAS_ERROR;
        }

        this.rColor = rColor;
        this.qColor = qColor;

        rList = new LinkedHashMap();
        qList = new LinkedHashMap();
        xList = new ArrayList();

        for (int referenceID : refIDs) {
            ArrayList<Xmap.Item> tempList = xMap.getMap(referenceID);

            HashMap<Integer, Cmap.Item> rCmapItem = rMap.get(referenceID);
            if (rCmapItem == null || rCmapItem.isEmpty()) {
                return CANVAS_ERROR;
            }

            rList.put(referenceID, rCmapItem);

            if (tempList != null) {
                int size = tempList.size();
                for (int i = 0; i < size; i++) {
                    Xmap.Item item = tempList.get(i);
                    boolean display = entryDisplayMap.get(item.entryID);
                    if (item.confidence > confidence && display) {
                        qList.put(item.qID, qMap.get(item.qID));
                        xList.add(item);
                    }
                }
            } else{
             xList.add(xMap.getNoMappingItem(rCmapItem));
            }
        }

        pc = new ScaffoldPositionCalculator(xList, canvasWidth, canvasHeight, minimumUnitLengh, minimumUnitNaturalLengh);

        setPreferredSize(new Dimension(pc.getWidth(), pc.getHeight()));
        return CANVAS_SUCCESS;
    }

    // return mark height
    // up means number is on the marks
    // left means 0 starts from left
    private int drawMarks(double length, int xMainOffset, int yMainOffset, boolean up, boolean left) {
        if (rulerText == false) {
            return 0;
        }
        smoothText();
        int marksCount = (int) (length / MILLION / minimumUnitLengh);
        int naturalLength = getRealLength(length);
        graph.setFont(new Font(font, fontStyle, getFontSize()));
        graph.setColor(fontColor);

        int mYbaseOffset = getFontSize() + yMainOffset + gapHight;
        int sYbaseOffset = getFontSize() + yMainOffset + mMarkHeight + gapHight - sMarkHeight;

        for (int i = 0; i <= marksCount; i++) {
            int xOffset = (int) (i * minimumUnitNaturalLengh);
            if (!left) {
                xOffset = naturalLength - xOffset;
            }
            if (i % 5 == 0) {
                double markValue = i * minimumUnitLengh;
                String markNumber;
                if (markValue == 0.0) {
                    markNumber = "0";
                } else {
                    markNumber = String.valueOf(markValue) + "M";
                }
                double middlePositionOffset = markNumber.length() * getXOffsetScale();
                if (markValue == 0.0) {
                    if (left) {
                        middlePositionOffset = 0;
                    }
                }
                if (up) {
                    graph.drawString(markNumber, (int) (xOffset - middlePositionOffset + xMainOffset), getFontSize() + yMainOffset);
                    graph.drawLine(xOffset + xMainOffset, mYbaseOffset, xOffset + xMainOffset, mYbaseOffset + mMarkHeight);
                } else {
                    graph.drawString(markNumber, (int) (xOffset - middlePositionOffset + xMainOffset), getFontSize() + yMainOffset + mMarkHeight);
                    graph.drawLine(xOffset + xMainOffset, yMainOffset, xOffset + xMainOffset, yMainOffset + mMarkHeight);
                }
            } else if (up) {
                graph.drawLine(xOffset + xMainOffset, sYbaseOffset, xOffset + xMainOffset, sYbaseOffset + sMarkHeight);
            } else {
                graph.drawLine(xOffset + xMainOffset, yMainOffset, xOffset + xMainOffset, yMainOffset + sMarkHeight);
            }

        }
        return (getFontSize() + mMarkHeight + gapHight);
    }

    private void drawReference(double length, int id, int[] position) {
        int xOffset = position[ScaffoldPositionCalculator.X_COORDINATE];
        int yOffset = position[ScaffoldPositionCalculator.Y_COORDINATE];

        // draw unit marks
        markHeight = drawMarks(length, xOffset, yOffset, true, true);

        yOffset += markHeight;

        int naturalLength = getRealLength(length);

        // draw reference bar
        setGradientPaint(xOffset, yOffset, barNaturalHeight, rColor);

        graph.fillRect(xOffset, yOffset, naturalLength, barNaturalHeight);

        drawID(id, naturalLength, xOffset, yOffset, true, true);
    }

    private void drawID(int RQid, int naturalLength, int xOffset, int yOffset, boolean left, boolean reference) {
        if (mapID == false) {
            return;
        }
        smoothText();
        graph.setFont(new Font(font, fontStyle, getFontSize()));
        graph.setColor(fontColor);
        String id = String.valueOf(RQid);

        if (left) {
            graph.drawString(id, xOffset, yOffset + getFontSize());
        } else {
            double x = xOffset + naturalLength - id.length() * getFontSize() * 0.7;
            graph.drawString(id, (int) x, yOffset + getFontSize());
        }
    }

    private void drawQuery(Xmap.Item item, int[] position) {
        int xOffset = position[ScaffoldPositionCalculator.X_COORDINATE];
        int yOffset = position[ScaffoldPositionCalculator.Y_COORDINATE];
        int naturalLength = getRealLength(item.qLen);
        boolean orientation = item.SSC_scaffold_orientation.equals(PLUS);

        // draw query bar
        setGradientPaint(xOffset, yOffset, barNaturalHeight, qColor);
        graph.fillRect(xOffset, yOffset, naturalLength, barNaturalHeight);
        drawID(item.qID, naturalLength, xOffset, yOffset, orientation, false);

        yOffset += barNaturalHeight;
        // draw unit marks
        drawMarks(item.qLen, xOffset, yOffset, false, orientation);
        drawScissors(xOffset, yOffset, xMap.queCutPoints.get(item.qID));
    }

    private void drawConnection(HashMap<Integer, Cmap.Item> rItemMap, HashMap<Integer, Cmap.Item> qItemMap, int rXPosition, int rYPosition, int qXPosition, int qYPosition, boolean rOrientation, boolean qOrientation, String alignment) {
        if (qItemMap == null)
            return;
        ArrayList<String> allMatches = new ArrayList();
        Matcher m = Pattern.compile("\\(\\d+,\\d+\\)").matcher(alignment);
        while (m.find()) {
            allMatches.add(m.group());
        }
        int size = allMatches.size();
        for (int i = 0; i < size; i++) {
            String couple = allMatches.get(i);
            String[] singles = couple.split(",");
            int rHitId = Integer.valueOf(singles[0].substring(1));
            int qHitId = Integer.valueOf(singles[1].substring(0, singles[1].length() - 1));

            Cmap.Item rItem = rItemMap.get(rHitId);
            double rTotalLength = rItem.length;

            Cmap.Item qItem = qItemMap.get(qHitId);
            double qTotalLength = qItem.length;

            int startX, startY, endX, endY;

            if (rOrientation) {
                startX = rXPosition + getRealLength(rItem.position);
            } else {
                startX = rXPosition + getRealLength(rTotalLength) - getRealLength(rItem.position);
            }

            startY = rYPosition + markHeight + barNaturalHeight;

            if (qOrientation) {
                endX = qXPosition + getRealLength(qItem.position);
            } else {
                endX = qXPosition + getRealLength(qTotalLength) - getRealLength(qItem.position);
            }

            endY = qYPosition;

            graph.setStroke(new BasicStroke(hitLineWidth));
            smoothLine();
            if (drawHitLines && matches) {
                graph.setColor(hitColor);
                graph.drawLine(startX, startY, endX, endY);
            }
            if (i == 0) {
                graph.setColor(START_LINE_COLOR);
                graph.drawLine(startX, startY, endX, endY);
            }
            if (i == size - 1) {
                graph.setColor(END_LINE_COLOR);
                graph.drawLine(startX, startY, endX, endY);
            }

        }

    }

    private void drawHitLine(boolean isReference, boolean orientation, double hitPosition, int xPosition, int yPosition, double totalLength, Color color, int lineWidth) {
        if (lineWidth == 0) {
            return;
        }
        smoothLine();
        int xStart, yStart;

        int hitNaturalPosition = getRealLength(hitPosition);
        int naturalTotalLength = getRealLength(totalLength);
        if (orientation) {
            xStart = hitNaturalPosition + xPosition;
        } else {
            xStart = xPosition + naturalTotalLength - hitNaturalPosition;
        }

        if (isReference) {
            yStart = yPosition + markHeight;
        } else {
            yStart = yPosition;
        }

        graph.setColor(color);
        graph.setStroke(new BasicStroke(lineWidth));
        graph.drawLine(xStart, yStart, xStart, yStart + barNaturalHeight);
    }

    private void drawAllHitLines(boolean isReference, boolean orientation, HashMap<Integer, Cmap.Item> itemMap, int xPosition, int yPosition, double totalLength) {
        if (drawHitLines && matches) {
            itemMap.entrySet().stream().forEach((entry) -> {
                drawHitLine(isReference, orientation, entry.getValue().position, xPosition, yPosition, totalLength, hitColor, hitLineWidth);
            });
        }
    }

    private void setGradientPaint(int x, int y, int height, Color color) {
        GradientPaint gp = new GradientPaint(x, y, color.brighter().brighter().brighter(), x, y + height, color);
        graph.setPaint(gp);
    }

    private void initComponent() {
        menuSaveAsSVG.addActionListener(this);
        menuSaveAsPNG.addActionListener(this);
        popupMenu.add(menuSaveAsSVG);
        popupMenu.add(menuSaveAsPNG);
        setAutoscrolls(true);
        addMouseListener(this);
        addMouseMotionListener(this);
    }

    private void saveAs(String format) {
        Dimension size = getSize();
        BufferedImage image = (BufferedImage) createImage(size.width, size.height);
        Graphics g = image.getGraphics();
        paint(g);
        g.dispose();

        JFileChooser saveDialog = getSaveDialog();

        if (saveDialog.showDialog(this, "Save") == JFileChooser.APPROVE_OPTION) {

            try {
                String fileName = saveDialog.getSelectedFile().getName();

                ImageIO.write(image, format, new File(saveDialog.getCurrentDirectory(), fileName + "." + format));

            } catch (IOException e) {
                //e.printStackTrace();
                System.out.println("Something went wrong while saving as " + format);
            }

        } else {
            //No Selection 
        }

    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (SwingUtilities.isRightMouseButton(e)) {
            popupMenu.show(e.getComponent(), e.getX(), e.getY());
        }

    }

    @Override
    public void mousePressed(MouseEvent e) {
        origin = new Point(e.getPoint());
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if (source == menuSaveAsSVG) {
            saveAsSVG();
        } else if (source == menuSaveAsPNG) {
            saveAs("png");
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (origin != null) {
            JViewport viewPort = (JViewport) SwingUtilities.getAncestorOfClass(JViewport.class, this);
            if (viewPort != null) {
                int deltaX = origin.x - e.getX();
                int deltaY = origin.y - e.getY();

                Rectangle view = viewPort.getViewRect();
                view.x += deltaX;
                view.y += deltaY;

                scrollRectToVisible(view);
            }
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    private JFileChooser getSaveDialog() {
        String userDir = System.getProperty("user.home");
        JFileChooser chooser = new JFileChooser(userDir + "/Desktop");
        chooser.setDialogTitle("Save Image");
        chooser.setDialogType(JFileChooser.SAVE_DIALOG);

        return chooser;
    }

    private void saveAsSVG() {
        DOMImplementation domImpl = GenericDOMImplementation.getDOMImplementation();

        String svgNS = "http://www.w3.org/2000/svg";
        Document document = domImpl.createDocument(svgNS, "svg", null);

        SVGGraphics2D svgGenerator = new SVGGraphics2D(document);

        paint(svgGenerator);

        JFileChooser saveDialog = getSaveDialog();

        if (saveDialog.showDialog(this, "Save") == JFileChooser.APPROVE_OPTION) {

            try {
                String fileName = saveDialog.getSelectedFile().getName();

                boolean useCSS = true; // we want to use CSS style attributes
                Writer out = new PrintWriter(new File(saveDialog.getCurrentDirectory(), fileName + ".svg"));
                svgGenerator.stream(out, useCSS);

            } catch (IOException e) {
                //e.printStackTrace();
                System.out.println("Something went wrong while saving as SVG");
            }

        } else {
            //No Selection 
        }
    }

    int getCanvasWidth() {
        return pc.totalWidth;
    }

    int getSelectedQueId(int x, int y) {
        return pc.getSelectedQueId(x, y);
    }

    void setWindowSize(int width, int height) {
        this.canvasWidth = width;
        this.canvasHeight = height;
    }

    void setEntryDisplay(int entryId, boolean display) {
        entryDisplayMap.put(entryId, display);
    }

    private void initEntryDisplayMap() {
        ArrayList<Xmap.Item> items = xMap.getAllItems();
        items.stream().forEach((item) -> {
            entryDisplayMap.put(item.entryID, true);
        });
    }

    void refresh() {
        initData(currentRefIDs, rColor, qColor);
    }

    void setColor(Color rColor, Color qColor) {
        this.rColor = rColor;
        this.qColor = qColor;
    }

    private void drawReference(Xmap.Item item, int[] position) {
        int xOffset = position[ScaffoldPositionCalculator.X_COORDINATE];
        int yOffset = position[ScaffoldPositionCalculator.Y_COORDINATE];
        int naturalLength = getRealLength(item.rLen);
        boolean orientation = item.SSC_orientation.equals(PLUS);

        // draw unit marks
        markHeight = drawMarks(item.rLen, xOffset, yOffset, true, orientation);
        yOffset += markHeight;

        // draw scissors
        drawScissors(xOffset, yOffset, xMap.refCutPoints.get(item.rID), true);
        
        // draw reference bar
        setGradientPaint(xOffset, yOffset, barNaturalHeight, rColor);
        graph.fillRect(xOffset, yOffset, naturalLength, barNaturalHeight);
        drawID(item.rID, naturalLength, xOffset, yOffset, orientation, true);
    }

    private int getRealLength(double length) {
        return getNaturalLength(length, minimumUnitLengh, minimumUnitNaturalLengh);
    }

    private void drawScissors(int xOffset, int yOffset, ArrayList<Double> cutPoints) {
        drawScissors(xOffset, yOffset, cutPoints, false);
    }

    private void drawScissors(int xOffset, int yOffset, ArrayList<Double> cutPoints, boolean flipped) {
        if (cutPoints == null) {
            return;
        }
        cutPoints.stream().map((point) -> getRealLength(point)).map((realPoint) -> xOffset + realPoint).forEach((x) -> {
            if (flipped == false) {
                graph.drawImage(scissors, x - SCISSOR_SIZE_W / 2, yOffset, null);
            } else {
                graph.drawImage(flipedScisseors, x - SCISSOR_SIZE_W / 2, yOffset - SCISSOR_SIZE_H, null);
            }

        });
    }
}
