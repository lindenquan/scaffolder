/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bionano;

import static com.bionano.ScaffoldConfig.*;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import javax.swing.JScrollPane;
import javax.swing.JViewport;

/**
 *
 * @author quanx
 */
public class ScaffoldCanvasScrollPane extends JScrollPane implements MouseWheelListener, ComponentListener {

    protected ScaffoldCanvas canvas;

    private final double NOTCH = 0.1;// unit is %

    private boolean isUpdating = false;

    int windowWidth = CANVAS_PANEL_WIDTH;
    int windowHeight = CANVAS_PANEL_HEIGHT;

    public ScaffoldCanvasScrollPane() {
        this.canvas = new ScaffoldCanvas();
        initComponents();
    }

    public void paint(Cmap rCmap, Cmap qCmap, Xmap xmap, int referenceID, Color rColor, Color qColor) {
        canvas.setMaps(rCmap, qCmap, xmap);
        canvas.setColor(rColor, qColor);
        updateCanvas(referenceID);
    }

    public void paintByGroupID(Cmap rCmap, Cmap qCmap, Xmap xmap, String groupID, Color rColor, Color qColor) {
        canvas.setMaps(rCmap, qCmap, xmap);
        updateCanvasByGroupID(groupID);
    }

    private void initComponents() {
        setBackground(Color.WHITE);
        setCanvasSize();
        setPreferredSize(new Dimension(CANVAS_PANEL_WIDTH, CANVAS_PANEL_HEIGHT));
        canvas.addMouseWheelListener(this);
        addMouseWheelListener(this);
        addComponentListener(this);
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        if (isUpdating == false) {
            isUpdating = true;
            int notches = e.getWheelRotation();

            // currentLength is 1M  natural length
            double currentLength = canvas.minimumUnitNaturalLengh / canvas.minimumUnitLengh;

            if (notches < 0) {
                // Mouse wheel moved UP
                currentLength *= (1 + NOTCH);
            } else {
                // Mouse wheel moved DOWN 
                currentLength *= (1 - NOTCH);
            }

            if (currentLength < zoom_min) {
                currentLength = zoom_min;
            }
            if (currentLength > zoom_max) {
                currentLength = zoom_max;
            }

            if (currentLength <= unit_change_threshold_1) {
                canvas.minimumUnitLengh = 0.2;
            } else if (currentLength <= unit_change_threshold_2) {
                canvas.minimumUnitLengh = 0.1;
            } else {
                canvas.minimumUnitLengh = 0.05;
            }
            canvas.minimumUnitNaturalLengh = currentLength * canvas.minimumUnitLengh;

            canvas.drawHitLines = currentLength >= hitline_threshold;

            int canvasWidthOld = canvas.getCanvasWidth();
            JViewport viewPort = getViewport();
            Point p = viewPort.getViewPosition();

            updateCanvas();

            int canvasWidthNew = canvas.getCanvasWidth();
            double scale = 1.0 * canvasWidthNew / canvasWidthOld - 1.0;

            int offset = (int) (scale * e.getX());

            int adjust = 7;
            if (scale > 0) {
                offset += adjust;
            } else if (scale < 0) {
                offset -= adjust;
            }

            p.x += offset;
            if (p.x > 0) {
                viewPort.setViewPosition(p);
            }

            isUpdating = false;
        }
    }

    void updateCanvas() {
        canvas.refresh();
        setViewportView(canvas);
        updateUI();
    }

    int updateCanvas(int referenceID) {
        int result = canvas.refresh(referenceID);

        if (result == CANVAS_ERROR) {
            return CANVAS_ERROR;
        }

        setViewportView(canvas);
        updateUI();
        return CANVAS_SUCCESS;
    }

    void updateCanvasByGroupID(String gid) {
        canvas.refreshByGroupID(gid);
        setViewportView(canvas);
        updateUI();
    }

    private void setCanvasSize() {
        int width = windowWidth - 2 * getVerticalScrollBar().getPreferredSize().width;
        int height = windowHeight - 2 * getHorizontalScrollBar().getPreferredSize().height;

        canvas.setWindowSize(width, height);
    }

    @Override
    public void componentResized(ComponentEvent e) {
        if (windowWidth == getWidth() && windowWidth == getHeight()) {
        } else {
            windowWidth = getWidth();
            windowHeight = getHeight();
            setCanvasSize();
            updateCanvas();
        }
    }

    @Override
    public void componentMoved(ComponentEvent e) {
    }

    @Override
    public void componentShown(ComponentEvent e) {
    }

    @Override
    public void componentHidden(ComponentEvent e) {
    }

    void addMouseListenerToCanvas(MouseListener mouthListener) {
        canvas.addMouseListener(mouthListener);
    }

    void setEntryDisplay(int entryId, boolean display) {
        canvas.setEntryDisplay(entryId, display);
        canvas.refresh();
        setViewportView(canvas);
        updateUI();
    }

}
