/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bionano;

import static com.bionano.ScaffoldConfig.BOTH_ENDED;
import static com.bionano.ScaffoldConfig.LEFT_ENDED;
import static com.bionano.ScaffoldConfig.PLUS;
import static com.bionano.ScaffoldConfig.RIGHT_ENDED;
import static com.bionano.ScaffoldConfig.SPLIT;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author quanx
 */
enum XmapMode {
    BNG, SUPER_CONTIG, CHROMOSOME, INVALID
}

public class Xmap {

    XmapMode mode;

    enum Column {
        entryID, qID, rID, qStartPos, qEndPos, rStartPos, rEndPos, orientation, confidence,
        hitEnum, qLen, rLen, labelChannel, alignment, edited, scaffoldedSuperContig,
        SSC_orientation, SSC_scaffold_orientation, chromosome, qryName, order,
        superContigCount, matchStatus, BNGendStatus, scaffoldEndStatus
    }

    public class Item {

        // BNG mode
        int entryID;                       //  0
        int qID;                           //  1
        int rID;                           //  2
        double qStartPos;                  //  3
        double qEndPos;                    //  4
        double rStartPos;                  //  5
        double rEndPos;                    //  6
        String orientation;                //  7
        double confidence;                 //  8
        String hitEnum;                    //  9
        double qLen;                       // 10
        double rLen;                       // 11
        int labelChannel;                  // 12 
        String alignment;                  // 13

        // SUPER_CONTIG mode
        boolean edited;                    // 14
        String scaffoldedSuperContig;      // 15
        String SSC_orientation = PLUS;            // 16
        String SSC_scaffold_orientation = PLUS;   // 17
        String qryName;                    // 19
        int order;                         // 20
        int superContigCount;              // 21
        String matchStatus = "";           // 22
        String BNGendStatus = "";          // 23
        String scaffoldEndStatus = "";     // 24

        // CHROMOSOME mode 
        String chromosome;                 // 18

        private Item getReversedClone() {
            Item item = new Item();
            item.entryID = this.entryID;
            item.qID = this.rID;
            item.rID = this.qID;
            item.qStartPos = this.rStartPos;
            item.qEndPos = this.rEndPos;
            item.rStartPos = this.qStartPos;
            item.rEndPos = this.qEndPos;
            item.orientation = this.orientation;
            item.confidence = this.confidence;
            item.hitEnum = this.hitEnum;
            item.rLen = this.qLen;
            item.qLen = this.rLen;
            item.labelChannel = this.labelChannel;
            item.alignment = getReversedAlignment(this.alignment);
            // super contig
            item.edited = this.edited;
            item.scaffoldedSuperContig = this.scaffoldedSuperContig;
            item.SSC_orientation = this.SSC_scaffold_orientation;
            item.SSC_scaffold_orientation = this.SSC_orientation;
            item.qryName = this.qryName;
            item.order = this.order;
            item.superContigCount = this.superContigCount;
            item.matchStatus = this.matchStatus;
            item.BNGendStatus = this.BNGendStatus;
            item.scaffoldEndStatus = this.scaffoldEndStatus;
            // choromosome
            item.chromosome = this.chromosome;
            return item;
        }

        private String getReversedAlignment(String alignment) {
            String reversed = "";

            ArrayList<String> allMatches = new ArrayList();
            Matcher m = Pattern.compile("\\(\\d+,\\d+\\)").matcher(alignment);
            while (m.find()) {
                allMatches.add(m.group());
            }
            int size = allMatches.size();
            for (int i = 0; i < size; i++) {
                String couple = allMatches.get(i);
                String[] singles = couple.split(",");
                int rHitId = Integer.valueOf(singles[0].substring(1));
                int qHitId = Integer.valueOf(singles[1].substring(0, singles[1].length() - 1));
                reversed += ("(" + qHitId + "," + rHitId + ")");
            }
            return reversed;
        }

        private Object[] getData() {
            Object[] data = new Object[Column.values().length];
            data[Column.entryID.ordinal()] = entryID;
            data[Column.qID.ordinal()] = qID;
            data[Column.rID.ordinal()] = rID;
            data[Column.qStartPos.ordinal()] = qStartPos;
            data[Column.qEndPos.ordinal()] = qEndPos;
            data[Column.rStartPos.ordinal()] = rStartPos;
            data[Column.rEndPos.ordinal()] = rEndPos;
            data[Column.orientation.ordinal()] = orientation;
            data[Column.confidence.ordinal()] = confidence;
            data[Column.hitEnum.ordinal()] = hitEnum;
            data[Column.rLen.ordinal()] = rLen;
            data[Column.qLen.ordinal()] = qLen;
            data[Column.labelChannel.ordinal()] = labelChannel;
            data[Column.alignment.ordinal()] = alignment;
            data[Column.edited.ordinal()] = edited;
            data[Column.scaffoldedSuperContig.ordinal()] = scaffoldedSuperContig;
            data[Column.SSC_orientation.ordinal()] = SSC_orientation;
            data[Column.SSC_scaffold_orientation.ordinal()] = SSC_scaffold_orientation;
            data[Column.chromosome.ordinal()] = chromosome;
            data[Column.qryName.ordinal()] = qryName;
            data[Column.order.ordinal()] = order;
            data[Column.superContigCount.ordinal()] = superContigCount;
            data[Column.matchStatus.ordinal()] = matchStatus;
            data[Column.BNGendStatus.ordinal()] = BNGendStatus;
            data[Column.scaffoldEndStatus.ordinal()] = scaffoldEndStatus;
            return data;
        }
    }

    private String[] columNames;
    private Object[][] data;
    private boolean isFoundColumn;
    private final String ENTRY_ID = "XmapEntryID";
    private LinkedHashMap<Integer, ArrayList<Item>> itemGroup = new LinkedHashMap();
    private LinkedHashMap<String, ArrayList<Integer>> groups = new LinkedHashMap();
    private int groupSize;
    private Xmap revseredXmap;
    private ArrayList<Item> allItems = new ArrayList();
    private String previousScaffoldedSuperContig = "";
    HashMap<Integer, ArrayList<Double>> refCutPoints = new HashMap();// key is reference Id
    HashMap<Integer, ArrayList<Double>> queCutPoints = new HashMap();// key is query Id

    private Xmap() {
    }

    public Xmap(File file) {
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        } catch (IOException e) {
            e.printStackTrace(new PrintWriter(System.out));
        }

        if (scanner != null) {
            while (scanner.hasNextLine()) {
                Item item = parseLine(scanner.nextLine());
                if (item != null) {
                    addItem(itemGroup, item);
                    addInGroup(groups, item);
                    allItems.add(item);
                }
            }
            scanner.close();
            extractData();
            groupSize = groups.size();
        }
    }

    private Item parseLine(String line) {
        String[] columns = line.split("\t");

        final int size = columns.length;

        if (size < 14) {
            return null;
        }

        if (isFoundColumn == false) {
            Pattern pattern = Pattern.compile(".*" + ENTRY_ID);
            Matcher matcher = pattern.matcher(columns[0]);
            if (matcher.matches()) {
                // found columns
                columNames = new String[size];
                columNames[0] = ENTRY_ID;
                System.arraycopy(columns, 1, columNames, 1, size - 1);
                isFoundColumn = true;
                mode = getMode(size);
                return null;
            }
        }

        Item item = new Item();

        try {
            item.entryID = Integer.parseInt(columns[Column.entryID.ordinal()]);
            item.qID = Integer.parseInt(columns[Column.qID.ordinal()]);
            item.rID = Integer.parseInt(columns[Column.rID.ordinal()]);
            item.qStartPos = Double.parseDouble(columns[Column.qStartPos.ordinal()]);
            item.qEndPos = Double.parseDouble(columns[Column.qEndPos.ordinal()]);
            item.rStartPos = Double.parseDouble(columns[Column.rStartPos.ordinal()]);
            item.rEndPos = Double.parseDouble(columns[Column.rEndPos.ordinal()]);
            item.orientation = columns[Column.orientation.ordinal()];
            item.confidence = Double.parseDouble(columns[Column.confidence.ordinal()]);
            item.hitEnum = columns[Column.hitEnum.ordinal()];
            item.qLen = Double.parseDouble(columns[Column.qLen.ordinal()]);
            item.rLen = Double.parseDouble(columns[Column.rLen.ordinal()]);
            item.labelChannel = Integer.parseInt(columns[Column.labelChannel.ordinal()]);
            item.alignment = columns[Column.alignment.ordinal()];
            item.SSC_orientation = PLUS;
            item.SSC_scaffold_orientation = item.orientation;
            tuneStartAndEnd(item);
        } catch (Exception e) {
            return null;
        }

        switch (mode) {
            case CHROMOSOME:
                item.chromosome = columns[Column.chromosome.ordinal()];
                if (item.chromosome.equals("")) {
                    mode = XmapMode.SUPER_CONTIG;
                }

            case SUPER_CONTIG:
                try {
                    item.edited = columns[Column.edited.ordinal()].toLowerCase().equals("true");
                    item.scaffoldedSuperContig = columns[Column.scaffoldedSuperContig.ordinal()];
                    item.SSC_orientation = columns[Column.SSC_orientation.ordinal()];
                    item.SSC_scaffold_orientation = columns[Column.SSC_scaffold_orientation.ordinal()];
                    item.qryName = columns[Column.qryName.ordinal()];
                    item.order = Integer.parseInt(columns[Column.order.ordinal()]);
                    item.superContigCount = Integer.parseInt(columns[Column.superContigCount.ordinal()]);
                    item.matchStatus = columns[Column.matchStatus.ordinal()];
                    item.BNGendStatus = columns[Column.BNGendStatus.ordinal()];
                    item.scaffoldEndStatus = columns[Column.scaffoldEndStatus.ordinal()];
                } catch (Exception e) {
                } finally {
                    findCutPoints(item);
                }
        }
        return item;
    }

    private void addItem(LinkedHashMap<Integer, ArrayList<Item>> itemGroup, Item item) {
        ArrayList items = itemGroup.get(item.rID);
        if (items == null) {
            items = new ArrayList();
            itemGroup.put(item.rID, items);
        }
        items.add(item);
    }

    public ArrayList<Item> getMap(int rID) {
        ArrayList<Item> list = itemGroup.get(rID);
        return list;
    }

    Xmap getReversed() {
        if (revseredXmap != null) {
            return revseredXmap;
        }
        revseredXmap = new Xmap();
        itemGroup.entrySet().stream().map((entry) -> entry.getValue()).forEach((ArrayList<Item> items) -> {
            int size = items.size();
            for (int i = 0; i < size; i++) {
                Item item = items.get(i).getReversedClone();
                addItem(revseredXmap.itemGroup, item);
                revseredXmap.allItems.add(item);
            }
        });
        revseredXmap.columNames = columNames;
        revseredXmap.extractData();
        revseredXmap.refCutPoints = queCutPoints;
        return revseredXmap;
    }

    String[] getColNames() {
        return columNames;
    }

    Object[][] getData() {
        return data;
    }

    // extract data from itemGroup
    private void extractData() {
        ArrayList<Item> items = new ArrayList();
        itemGroup.entrySet().stream().forEach((entry) -> {
            items.addAll(entry.getValue());
        });

        final int size = items.size();
        final int columLength = columNames.length;
        data = new Object[size][columLength];

        for (int i = 0; i < size; i++) {
            Object[] itemData = items.get(i).getData();
            System.arraycopy(itemData, 0, data[i], 0, columLength);
        }
    }
    Xmap.Item getNoMappingItem(HashMap<Integer, Cmap.Item> cmap){
        Xmap.Item item = new Xmap.Item();
        Cmap.Item cmapItem = cmap.get(1);
        item.rLen = cmapItem.length;
        item.rID = cmapItem.id;
        item.qID = -1;
        return item;
    }
    
    Xmap getByID(int id, double confidence) {
        ArrayList<Integer> list = new ArrayList();
        list.add(id);
        return getByID(list, confidence);
    }

    Xmap getByID(ArrayList<Integer> ids, double confidence) {
        Xmap map = new Xmap();
        ids.stream().map((refID) -> itemGroup.get(refID))
                    .forEach((items) -> {
                      if (items == null) {
                      return;
                      }
                      items.stream()
                      .filter((item) -> (item.confidence >= confidence))
                      .forEach((item) -> {
                        addItem(map.itemGroup, item);
                       });
        });
        map.columNames = columNames;
        map.extractData();
        return map;
    }

    Xmap getByGroupID(String gid, double confidence) {
        return getByID(getRefIDsByGID(gid), confidence);
    }

    ArrayList<Item> getAllItems() {
        return allItems;
    }

    private XmapMode getMode(int size) {
        int totalColumns = Column.values().length;

        if (totalColumns == size) {
            return XmapMode.CHROMOSOME;
        } else if (size == (totalColumns - 1)) {
            return XmapMode.SUPER_CONTIG;
        }
        return XmapMode.BNG;
    }

    int getGroupSize() {
        return groupSize;
    }

    private void addInGroup(LinkedHashMap<String, ArrayList<Integer>> groups, Item item) {
        if (mode == XmapMode.SUPER_CONTIG || mode == XmapMode.CHROMOSOME) {
            String currentContig = item.scaffoldedSuperContig;
            if (!previousScaffoldedSuperContig.equals(currentContig)) {
                String[] refIDs = item.scaffoldedSuperContig.split("\\+");
                ArrayList<Integer> list = new ArrayList();

                int size = refIDs.length;
                for (int i = 0; i < size; i++) {
                    int refID = Integer.valueOf(refIDs[i]);
                    list.add(refID);
                }
                groups.put(currentContig, list);
                previousScaffoldedSuperContig = currentContig;
            }
        }
    }

    ArrayList<Integer> getRefIDsByGID(String gid) {
        ArrayList<Integer> refIDs = groups.get(gid);
        return refIDs;
    }

    String getFirstGroupID() {
        return groups.keySet().iterator().next();
    }

    String[] getGroupNames() {
        Set<String> names = groups.keySet();
        return names.toArray(new String[names.size()]);
    }

    private void findCutPoints(Item item) {
        int qid = item.qID;
        ArrayList<Double> points = queCutPoints.get(qid);
        if (points == null) {
            points = new ArrayList();
            queCutPoints.put(qid, points);
        }

        if (item.matchStatus.equals(SPLIT)) {

            boolean refOrientation = item.SSC_orientation.equals(PLUS);
            boolean refRight = false;
            boolean refLeft = false;

            switch (item.BNGendStatus) {
                case BOTH_ENDED:
                    // do nothing
                    break;
                case RIGHT_ENDED:
                    if (refOrientation) {
                        refLeft = true;
                    } else {
                        refRight = true;
                    }
                    break;
                case LEFT_ENDED:
                    if (refOrientation) {
                        refRight = true;
                    } else {
                        refLeft = true;
                    }
                    break;
                default:
                    refRight = true;
                    refLeft = true;
                    break;
            }

            boolean orientation = item.SSC_scaffold_orientation.equals(PLUS);
            double start = item.qStartPos;
            double end = item.qEndPos;
            double length = item.qLen;
            if (orientation == false) {
                start = length - start;
                end = length - end;
            }

            switch (item.scaffoldEndStatus) {
                case BOTH_ENDED:
                    // do nothing
                    break;
                case RIGHT_ENDED:
                    if (orientation && refLeft) {
                        points.add(start);
                    } else if (!orientation && refRight) {
                        points.add(start);
                    }
                    break;
                case LEFT_ENDED:
                    if (orientation && refRight) {
                        points.add(end);
                    } else if (!orientation && refLeft) {
                        points.add(end);
                    }
                    break;
                default:
                    if (orientation) {
                        if (refLeft) {
                            points.add(start);
                        }
                        if (refRight) {
                            points.add(end);
                        }
                    } else {
                        if (refLeft) {
                            points.add(end);
                        }
                        if (refRight) {
                            points.add(start);
                        }
                    }
                    break;
            }
        }
    }

    private void tuneStartAndEnd(Item item) {
        if (item.qStartPos > item.qEndPos) {
            double temp = item.qStartPos;
            item.qStartPos = item.qEndPos;
            item.qEndPos = temp;
            temp = item.rStartPos;
            item.rStartPos = item.rEndPos;
            item.rEndPos = temp;
        }
    }
}
