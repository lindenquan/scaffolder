/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bionano;

import static com.bionano.ScaffoldConfig.*;
import static com.bionano.Tool.getNaturalLength;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author quanx
 */
public class ScaffoldPositionCalculator {

    class Section {

        int start;
        int end;

        Section(int start, int end) {
            this.start = start;
            this.end = end;
        }
    }

    static final int X_COORDINATE = 0;
    static final int Y_COORDINATE = 1;
    static final int LENGTH = 2;

    int windowWidth;
    int windowHeight;

    ArrayList<Xmap.Item> xList;

    HashMap<Integer, int[]> quePositions = new HashMap();
    HashMap<Integer, int[]> refPositions = new HashMap();

    int refTotalWidth;

    final int barTotalHeight = getFontSize() + gapHight + mMarkHeight + barNaturalHeight;

    int smallestQueXOffset;
    int biggestQueXOffset;
    int biggestQueYOffset;
    int biggestRefYoffset;

    int totalWidth;
    int totalHeight;

    boolean isQueExist = false;

    HashMap<Integer, ArrayList<Section>> queLayers = new HashMap();
    HashMap<Integer, ArrayList<Section>> refLayers = new HashMap();

    double minimumUnitLengh, minimumUnitNaturalLengh;

    ScaffoldPositionCalculator(ArrayList<Xmap.Item> xList, int windowWidth, int windowHeight, double minimumUnitLengh, double minimumUnitNaturalLengh) {
        this.xList = xList;
        this.windowWidth = windowWidth;
        this.windowHeight = windowHeight;
        this.minimumUnitLengh = minimumUnitLengh;
        this.minimumUnitNaturalLengh = minimumUnitNaturalLengh;
        calculate();
        centralize();
    }

    private void calculate() {
        calcPositions();

        totalWidth = refTotalWidth > biggestQueXOffset ? refTotalWidth : biggestQueXOffset;

        if (smallestQueXOffset < 0) {
            totalWidth -= smallestQueXOffset;
        }
    }

    int[] getRefPosition(int id) {
        return refPositions.get(id);
    }

    int[] getQuePosition(int id) {
        return quePositions.get(id);
    }

    int getWidth() {
        return totalWidth;
    }

    int getHeight() {
        return totalHeight;
    }

    int getRealLength(double length) {
        return getNaturalLength(length, minimumUnitLengh, minimumUnitNaturalLengh);
    }

    private int calcXPos(int fixedBegin, int fixedLength, double fixedSt, boolean fixedOrientation,
            double floatSt, int floatLength, boolean floatOrientation) {

        int floatBegin;
        int fixedStart = getRealLength(fixedSt);
        int floatStart = getRealLength(floatSt);

        // calcuate qPos[X]
        if (fixedOrientation && floatOrientation) {
            floatBegin = fixedBegin + fixedStart - floatStart;
        } else if (fixedOrientation && !floatOrientation) {
            floatBegin = fixedBegin + fixedStart - (floatLength - floatStart);
        } else if (!fixedOrientation && floatOrientation) {
            floatBegin = fixedBegin + fixedLength - fixedStart - floatStart;
        } else {
            floatBegin = fixedBegin + (fixedLength - fixedStart) - (floatLength - floatStart);
        }
        return floatBegin;
    }

    private void calcRefPosition(Xmap.Item item) {
        int[] pos = new int[2];
        int qID = item.qID;
        int[] quePos = quePositions.get(qID);
        int qLen = getRealLength(item.qLen);
        int rLen = getRealLength(item.rLen);
        // calcuate x coordinate
        if (quePos == null) {
            pos[X_COORDINATE] = refTotalWidth + gapRefRefHorizontal;
        } else {
            boolean fixedOrientation = item.SSC_scaffold_orientation.equals(PLUS);
            boolean floatOrientation = item.SSC_orientation.equals(PLUS);
            pos[X_COORDINATE] = calcXPos(quePos[X_COORDINATE], qLen, item.qStartPos, fixedOrientation, item.rStartPos, rLen, floatOrientation);
        }
        int mostRefXoffset = pos[X_COORDINATE] + rLen;
        if (mostRefXoffset> refTotalWidth)
            refTotalWidth = mostRefXoffset;
        
        // calculate y coordinate
        pos[Y_COORDINATE] = calcYPos(refLayers, pos[X_COORDINATE], rLen, gapRefRefHorizontal, true);

        refPositions.put(item.rID, pos);

        // update biggestRefYoffset
        int y = pos[Y_COORDINATE];
        biggestRefYoffset = y < biggestRefYoffset ? y : biggestRefYoffset;
    }

    private void updateQueSize(int[] pos, int len) {
        // update smallest que x offset
        if (pos[X_COORDINATE] < smallestQueXOffset) {
            smallestQueXOffset = pos[X_COORDINATE];
        }

        // update biggest  que x offset
        int queXoffset = pos[X_COORDINATE] + len;
        biggestQueXOffset = biggestQueXOffset > queXoffset ? biggestQueXOffset : queXoffset;

        // update biggest que Y offset
        if (pos[Y_COORDINATE] + barTotalHeight > biggestQueYOffset) {
            biggestQueYOffset = pos[Y_COORDINATE] + barTotalHeight;
        }
    }

    private void calcQuePosition(Xmap.Item item) {
        int[] pos = new int[3];

        int rID = item.rID;
        int qID = item.qID;
        int rLen = getRealLength(item.rLen);
        int qLen = getRealLength(item.qLen);
        int rPosX = refPositions.get(rID)[X_COORDINATE];
        boolean rOrientation = item.SSC_orientation.equals(PLUS);
        boolean qOrientation = item.SSC_scaffold_orientation.equals(PLUS);

        // calculate x coordinate
        pos[X_COORDINATE] = calcXPos(rPosX, rLen, item.rStartPos, rOrientation, item.qStartPos, qLen, qOrientation);

        // calculate y coordinate
        int y = calcYPos(queLayers, pos[X_COORDINATE], qLen, gapHorizontal, false);
        pos[Y_COORDINATE] = barTotalHeight + gapRefQue + y;

        pos[LENGTH] = qLen;
        quePositions.put(qID, pos);

        updateQueSize(pos, qLen);
    }

    // calculate all que positions corresponding to rID
    private void calcQuePositions(int targetRefID) {
        int size = xList.size();

        for (int i = 0; i < size; i++) {
            Xmap.Item item = xList.get(i);
            int rID = item.rID;
            int qID = item.qID;

            if (targetRefID != rID || quePositions.containsKey(qID) || qID <0) {
                continue;
            }
            calcQuePosition(item);
        }
    }

    private void calcPositions() {
        int size = xList.size();
        for (int i = 0; i < size; i++) {
            Xmap.Item item = xList.get(i);
            int rID = item.rID;
            int[] rPos = refPositions.get(rID);
            if (rPos == null) {
                calcRefPosition(item);
                calcQuePositions(rID);
            }
        }
    }

    private void centralize() {
        final int xOffset, yOffset;
        
        totalHeight = biggestQueYOffset - biggestRefYoffset;
        
        int marginYOffset = (windowHeight - totalHeight) / 2;

        if (marginYOffset < margin) {
            marginYOffset = margin;
        }

        yOffset = marginYOffset - biggestRefYoffset;

        totalHeight += (2 * yOffset);

        int marginXOffset = (windowWidth - totalWidth) / 2;

        if (marginXOffset < margin) {
            marginXOffset = margin;
        }

        if (smallestQueXOffset < 0) {
            xOffset = marginXOffset - smallestQueXOffset;
        } else {
            xOffset = marginXOffset;
        }

        totalWidth += (2 * marginXOffset);

        quePositions.entrySet().stream().forEach((entry) -> {
            entry.getValue()[X_COORDINATE] += xOffset;
            entry.getValue()[Y_COORDINATE] += yOffset;
        });

        refPositions.entrySet().stream().forEach((entry) -> {
            entry.getValue()[X_COORDINATE] += xOffset;
            entry.getValue()[Y_COORDINATE] += yOffset;
        });

    }

    private int calcYPos(HashMap<Integer, ArrayList<Section>> layers, int xBegin, int len, int gap, boolean upward) {
        int size = layers.size();
        int layer = -1;
        int xEndPos = xBegin + len + gap;
        for (int i = 0; i <= size; i++) {
            ArrayList<Section> sections = layers.get(i);
            if (sections == null) {
                sections = new ArrayList();
                sections.add(new Section(xBegin, xEndPos));
                layers.put(i, sections);
                layer = i;
                break;
            } else {
                int secSize = sections.size();
                int cleanNum = 0;
                for (int j = 0; j < secSize; j++) {
                    Section section = sections.get(j);
                    if (section.end <= xBegin || xEndPos <= section.start) {
                        cleanNum++;
                    }
                }

                if (secSize == cleanNum) {
                    sections.add(new Section(xBegin, xEndPos));
                    layer = i;
                    break;
                }
            }
        }

        int y = layer * (barTotalHeight + gapVertical);
        if (upward) {
            return -y;
        } else {
            return y;
        }
    }

    int getSelectedQueId(int x, int y) {
        int queId = 0;

        for (Map.Entry<Integer, int[]> entry : quePositions.entrySet()) {
            int xStart = entry.getValue()[X_COORDINATE];
            int yStart = entry.getValue()[Y_COORDINATE];
            int length = entry.getValue()[LENGTH];
            if (x >= xStart && x <= length + xStart) {
                if (y >= yStart && y <= yStart + barTotalHeight) {
                    queId = entry.getKey();
                }
            }
        }

        return queId;
    }
}
