/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bionano;

import com.bionano.component.LoadingPanel;
import com.bionano.event.SplitPaneListener;
import java.awt.Dimension;
import javax.swing.JSplitPane;

import static com.bionano.ScaffoldConfig.*;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import static java.awt.image.ImageObserver.ERROR;
import javax.swing.SwingUtilities;

/**
 *
 * @author quanx
 */
public class ScaffoldSplitPane extends JSplitPane {

    private final ScaffoldCanvasScrollPane mainCanvasSP = new ScaffoldCanvasScrollPane();
    private final ScaffoldCanvasScrollPane viceCanvasSP = new ScaffoldCanvasScrollPane();

    private boolean isSetBottom = false;

    final private SplitPaneListener spListener;

    class SplitPanMouseAdapter extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent e) {
            if (SwingUtilities.isRightMouseButton(e)) {
                return;
            }

            Object source = e.getSource();
            if (source == mainCanvasSP.canvas) {
                int selectedRefId = mainCanvasSP.canvas.getSelectedQueId(e.getX(), e.getY());

                if (selectedRefId == 0) {
                    return;
                }
                int result = updateViceCanvas(selectedRefId);
                if (result == ERROR) {
                    return;
                }

                Dimension d = mainCanvasSP.getPreferredSize();
                if (isSetBottom == false) {
                    Dimension size = viceCanvasSP.getPreferredSize();
                    size.height = getPreferredSize().height - d.height;
                    viceCanvasSP.setPreferredSize(size);
                    setBottomComponent(viceCanvasSP);
                    setDividerLocation(d.height /= 2);
                    isSetBottom = true;
                } else {
                    setDividerLocation(d.height /= 2);
                }
                spListener.onSplitPaneClick(selectedRefId, confidence);
            }
        }
    }

    ScaffoldSplitPane(SplitPaneListener spListener) {
        super(JSplitPane.VERTICAL_SPLIT);
        super.setOneTouchExpandable(true);
        super.setTopComponent(new LoadingPanel(CANVAS_PANEL_WIDTH, CANVAS_PANEL_HEIGHT));
        mainCanvasSP.addMouseListenerToCanvas(new SplitPanMouseAdapter());
        this.spListener = spListener;
    }

    void updateMainCanvas(int referenceID) {
        mainCanvasSP.updateCanvas(referenceID);
    }

    int updateViceCanvas(int referenceID) {
        return viceCanvasSP.updateCanvas(referenceID);
    }

    void updateViceCanvas() {
        viceCanvasSP.updateCanvas();
    }

    private void setTopCanvas() {
        Component topComopnent = getTopComponent();
        if (topComopnent != mainCanvasSP) {
            remove(topComopnent);
            setTopComponent(mainCanvasSP);
        }
    }

    void setMainCanvas(Cmap rCmap, Cmap qCmap, Xmap xmap, int referenceID) {
        mainCanvasSP.paint(rCmap, qCmap, xmap, referenceID, GREENISH, BLUEISH);
        setTopCanvas();
    }

    void setMainCanvasByGroupID(Cmap rCmap, Cmap qCmap, Xmap xmap, String groupID) {
        mainCanvasSP.paintByGroupID(rCmap, qCmap, xmap, groupID, GREENISH, BLUEISH);
        setTopCanvas();
    }

    void setViceCanvas(Cmap rCmap, Cmap qCmap, Xmap xmap, int referenceID) {
        viceCanvasSP.paint(rCmap, qCmap, xmap, referenceID, BLUEISH, GREENISH);
    }

    void updateMainCanvas(int entryId, boolean display) {
        mainCanvasSP.setEntryDisplay(entryId, display);

    }

    void updateViceCanvas(int entryId, boolean display) {
        viceCanvasSP.setEntryDisplay(entryId, display);
    }

    void updateMainCanvasByGroup(String gid) {
        mainCanvasSP.updateCanvasByGroupID(gid);
    }

    void updateMainCanvas() {
        mainCanvasSP.updateCanvas();
    }
}
