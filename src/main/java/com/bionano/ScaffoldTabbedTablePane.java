/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bionano;

import com.bionano.event.TableSetListener;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

/**
 *
 * @author quanx
 */
public class ScaffoldTabbedTablePane extends JPanel {
    
    class TabMouseAdapter extends MouseAdapter {
        
        @Override
        public void mouseClicked(MouseEvent e) {
            int x = e.getX();
            int y = e.getY();
            int paneWidth = getWidth();
            int buttonLeft = paneWidth - SAVE_BUTTON_TOTAL_WIDTH;
            int buttonRight = buttonLeft + SAVE_BUTTON_REAL_WIDTH;
            
            if (y <= SAVE_BUTTON_HEIGHT && x >= buttonLeft && x <= buttonRight) {
                saveButton.doClick();
            }
        }
    }
    
    JTabbedPane tabbedPane = new JTabbedPane();
    
    JButton saveButton = new JButton("Save");
    
    final int SAVE_BUTTON_TOTAL_WIDTH = 100;
    final int SAVE_BUTTON_REAL_WIDTH = 80;
    final int SAVE_BUTTON_HEIGHT = 30;
    
    ScaffoldTable firstTable, secondTable;
    
    ArrayList<TableSetListener> tableSetListeners = new ArrayList();
    
    ScaffoldTabbedTablePane(int width, int height) {
        super.setLayout(null);
        super.setPreferredSize(new Dimension(width, height));
        
        tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
        tabbedPane.addMouseListener(new TabMouseAdapter());
        
        super.add(tabbedPane);
        super.add(saveButton);
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        int width = getWidth();
        int height = getHeight();
        saveButton.setBounds(width - SAVE_BUTTON_TOTAL_WIDTH, 0, SAVE_BUTTON_REAL_WIDTH, SAVE_BUTTON_HEIGHT);
        tabbedPane.setBounds(0, 0, width, height);
    }
    
    protected JComponent makeTextPanel(String text) {
        JPanel panel = new JPanel(false);
        JLabel filler = new JLabel(text);
        filler.setHorizontalAlignment(JLabel.CENTER);
        panel.setLayout(new GridLayout(1, 1));
        panel.add(filler);
        return panel;
    }
    
    void setFirstTable(Xmap xmap, String tabTitle) {
        if (firstTable == null) {
            firstTable = new ScaffoldTable(xmap);
            firstTable.addTableChangeListener((int entryId, boolean data) -> {
                tableSetListeners.stream().forEach((listener) -> {
                    listener.onFirstTableSet(entryId, data);
                });
            });
            
            tabbedPane.addTab(tabTitle, firstTable);
        } else {
            tabbedPane.setTitleAt(0, tabTitle);
            firstTable.update(xmap);
        }
    }
    
    void setSecondTable(Xmap xmap, String tabTitle) {
        if (secondTable == null) {
            secondTable = new ScaffoldTable(xmap);
            secondTable.addTableChangeListener((int entryId, boolean data) -> {
                tableSetListeners.stream().forEach((listener) -> {
                    listener.onSecondTableSet(entryId, data);
                });
            });
            
            tabbedPane.addTab(tabTitle, secondTable);
        } else {
            secondTable.update(xmap);
        }
    }
    
    void setSelectedIndex(int index) {
        tabbedPane.setSelectedIndex(index);
    }
    
    void addTableSetListener(TableSetListener tableSetListener) {
        tableSetListeners.add(tableSetListener);
    }
}
