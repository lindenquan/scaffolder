/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bionano;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author quanx
 */
public class Cmap {

    class Item {
        int id;
        double length;
        int numSites;
        int siteID;
        int labelChannel;
        double position;
        double stdDev;
        double coverage;
        double occurrence;
    }

    private HashMap<Integer, HashMap<Integer, Item>> itemGroups = new HashMap();

    public Cmap(File file) {
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        } catch (IOException e) {
            e.printStackTrace(new PrintWriter(System.out));
        }

        if (scanner != null) {
            while (scanner.hasNextLine()) {
                Item item = parseLine(scanner.nextLine());
                if (item != null) {
                    addItem(itemGroups, item);
                }
            }
            scanner.close();
        }
    }

    private Cmap() {
    }

    private void addItem(HashMap<Integer, HashMap<Integer, Item>> itemGroups, Item item) {
        HashMap<Integer, Item> items = itemGroups.get(item.id);
        if (items == null) {
            items = new HashMap();
            itemGroups.put(item.id, items);
        }
        items.put(item.siteID, item);
    }

    private Item parseLine(String line) {
        if (line.charAt(0) == '#') {
            return null;
        }

        String[] columns = line.split("\t");

        Item item = new Item();

        try {
            item.id = Integer.parseInt(columns[0]);
            item.length = Double.parseDouble(columns[1]);
            item.numSites = Integer.parseInt(columns[2]);
            item.siteID = Integer.parseInt(columns[3]);
            item.labelChannel = Integer.parseInt(columns[4]);
            item.position = Double.parseDouble(columns[5]);
            item.stdDev = Double.parseDouble(columns[6]);
            item.coverage = Double.parseDouble(columns[7]);
            item.occurrence = Double.parseDouble(columns[8]);
            return item;

        } catch (NumberFormatException e) {
            e.printStackTrace(new PrintWriter(System.out));
            return null;
        }
    }

    Item getFirst(int id) {
        return itemGroups.get(id).get(1);
    }

    HashMap<Integer, Item> get(int id) {
        return itemGroups.get(id);
    }
    
    Item getItem(int id){
        return itemGroups.get(id).get(1);
    }
    
    int getSize() {
        return itemGroups.size();
    }

    Cmap getClone() {
        Cmap map = new Cmap();
        itemGroups.entrySet().stream().map((entry) -> entry.getValue()).forEach((HashMap<Integer, Item> subItemGroups) -> {
            subItemGroups.entrySet().stream().map((entry) -> entry.getValue()).forEach((Item item) -> {
                addItem(map.itemGroups, item);
            });
        });
        
        return map;
    }
    
    int getFirstID() {
        return 1;
    }
}
