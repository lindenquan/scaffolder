/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bionano.component;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import javax.swing.JButton;

/**
 *
 * @author quanx
 */
public class GradientButton extends JButton {

    String buttonName = null;
    boolean on;

    public GradientButton(String buttonName, boolean on) {
        super(buttonName);
        this.buttonName = buttonName;
        this.on = on;
    }

    {
        setContentAreaFilled(false);
    }

    @Override
    protected void paintComponent(Graphics g) {
        final Graphics2D g2 = (Graphics2D) g.create();
        if (on) {
            g2.setPaint(new GradientPaint(
                    new Point(0, 0),
                    Color.WHITE,
                    new Point(0, getHeight()),
                    new Color(51, 153, 255)));

        } else {
            g2.setPaint(new GradientPaint(
                    new Point(0, 0),
                    Color.WHITE,
                    new Point(0, getHeight()),
                    new Color(120, 120, 120)));

        }
        g2.fillRect(0, 0, getWidth(), getHeight());
        g2.dispose();
        super.paintComponent(g);
    }

    public void repaint(boolean on) {
        this.on = on;
        repaint();
    }

}
