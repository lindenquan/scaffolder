/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bionano.component;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author quanx
 */
public class LoadingPanel extends JPanel {
    
    public LoadingPanel(int width, int height){
        this(new Dimension(width, height));
    }
    
    public  LoadingPanel(Dimension dimension) {
        super.setLayout(new GridBagLayout());
        super.setPreferredSize(dimension);
        ImageIcon loading = new ImageIcon(LoadingPanel.class.getResource("/img/loading.gif"));
        super.add(new JLabel("loading... ", loading, JLabel.CENTER));
        super.setBackground(Color.WHITE);
    }
}
