/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bionano;

import static com.bionano.ScaffoldConfig.MILLION;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

/**
 *
 * @author quanx
 */
public class Tool {

    static int getNaturalLength(double length, double minimumUnitLengh, double minimumUnitNaturalLengh) {
        return (int) (length / MILLION / minimumUnitLengh * minimumUnitNaturalLengh);
    }

    static void print(Object o) {
        System.out.println(o.toString());
    }

    static BufferedImage createFlipped(BufferedImage image) {
        AffineTransform at = AffineTransform.getScaleInstance(1, -1);
        at.translate(0, -image.getHeight(null));
        return new AffineTransformOp(at, AffineTransformOp.TYPE_NEAREST_NEIGHBOR).filter(image, null);
    }

    static BufferedImage createScaled(BufferedImage image, int width, int height) {
        int w = image.getWidth();
        int h = image.getHeight();
        AffineTransform at = new AffineTransform();
        at.scale(1.0 * width / w, 1.0 * height / h);
        return new AffineTransformOp(at, AffineTransformOp.TYPE_NEAREST_NEIGHBOR).filter(image, null);
    }
}
