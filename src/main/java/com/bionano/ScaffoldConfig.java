/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bionano;

import java.awt.Color;
import java.awt.Font;

/**
 *
 * @author quanx
 */
public class ScaffoldConfig {

    final static String CHROMOSOME = "Chromosome";
    final static String GROUP = "Group";
    final static String BNG = "BNG";
    final static String RIGHT_ENDED = "Right ended";
    final static String LEFT_ENDED = "Left ended";
    final static String BOTH_ENDED = "Both sides ended";
    final static String SPLIT = "Split";

    final static int MILLION = 1000000;

    final static String PLUS = "+";
    final static int CANVAS_ERROR = -1;
    final static int CANVAS_SUCCESS = 0;

    final static int CANVAS_PANEL_HEIGHT = 600;
    final static int CANVAS_PANEL_WIDTH = 1200;

    final static int TABLE_PANEL_WIDTH = CANVAS_PANEL_WIDTH;
    final static int TABLE_PANEL_HEIGHT = 300;

    final static Color GREENISH = new Color(120, 205, 0, 200);
    final static Color BLUEISH = new Color(25, 100, 245, 200);

    static Color START_LINE_COLOR = new Color(24, 134, 45);
    static Color END_LINE_COLOR = new Color(246, 70, 91);
    static int StartEndLineWidth = 1;

    static boolean rulerText = true; // ture means display ruler text
    static boolean mapID = true; // true mean display id number
    static boolean matches = true; //  true mean display mathces
    static boolean smoothLine = false; // true means display smooth line

    private static int fontSize;
    static int barNaturalHeight = 35;
    static int confidence = 10;

    static Color hitColor = new Color(100, 100, 100, 170);
    static int hitLineWidth = 1;

    static String font = "Arial";
    static Color fontColor = new Color(0, 51, 102);
    static int fontStyle = Font.BOLD;

    static int sMarkHeight = 5;
    static int mMarkHeight = 9;
    static int gapHight = 2; // gap between number and marks
    private static double xOffsetScale;

    static int gapHorizontal = 5; // horizontal gap between qry and qry
    static int gapVertical = 10; // vertical gap between qry and qry also between ref and ref

    static int gapRefRefHorizontal = 5; // horizontal gap between ref and ref

    static int margin = 50;
    static int gapRefQue = 50;

    static double hitline_threshold = 500;

    static void setFontSize(int size) {
        fontSize = size;
        xOffsetScale = size / 3;
    }

    static int getFontSize() {
        return fontSize;
    }

    static double getXOffsetScale() {
        return xOffsetScale;
    }

    static double unit_change_threshold_1 = 70; // unit is px
    static double unit_change_threshold_2 = 200; // unit is px

    static double zoom_max = 5000;
    static double zoom_min = 50;

    static {
        setFontSize(15);
    }
    
    final static int SCISSOR_SIZE_W = 12;
    final static int SCISSOR_SIZE_H = 12;
}
