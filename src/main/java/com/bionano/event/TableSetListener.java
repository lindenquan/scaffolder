/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bionano.event;

/**
 *
 * @author quanx
 */
public interface TableSetListener {

    public void onFirstTableSet(int entryId, boolean data);

    public void onSecondTableSet(int entryId, boolean data);
    
}
