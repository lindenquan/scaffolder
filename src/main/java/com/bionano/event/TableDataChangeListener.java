/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bionano.event;

/**
 *
 * @author quanx
 */
public interface TableDataChangeListener {

    public void onTableDataChange(int entryId, boolean data);
    
}
