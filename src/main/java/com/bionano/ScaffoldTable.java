/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bionano;

import com.bionano.event.TableDataChangeListener;
import com.jidesoft.converter.ConverterContext;
import com.jidesoft.grid.BooleanCheckBoxCellEditor;
import com.jidesoft.grid.ContextSensitiveTableModel;
import com.jidesoft.grid.EditorContext;
import com.jidesoft.grid.SortableTable;
import java.awt.GridLayout;
import java.util.ArrayList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author quanx
 */
public class ScaffoldTable extends JPanel {

    private final static int DISPLAY_COLUMN = 0;

    private class ScaffoldTableModel extends DefaultTableModel implements ContextSensitiveTableModel, TableModelListener {

        {
            addTableModelListener(this);
        }

        public ScaffoldTableModel(Object[][] data, Object[] columnNames) {
            super(data, columnNames);
        }

        @Override
        public Class<?> getColumnClass(int column) {

            if (column == DISPLAY_COLUMN) {
                return Boolean.class;
            }
            return Object.class;
        }

        @Override
        public EditorContext getEditorContextAt(int row, int column) {
            if (column == DISPLAY_COLUMN) {
                return BooleanCheckBoxCellEditor.CONTEXT;
            }
            return null;
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return columnIndex == DISPLAY_COLUMN;
        }

        @Override
        public void tableChanged(TableModelEvent e) {
            int column = e.getColumn();

            if (column == 0) {
                int row = e.getFirstRow();
                TableModel model = (TableModel) e.getSource();
                //String columnName = model.getColumnName(column);
                boolean data = (boolean) model.getValueAt(row, column);
                int entryId = (int) model.getValueAt(row, column + 1);
                notifyDataChange(entryId, data);
            }
        }

        private void notifyDataChange(int entryId, boolean data) {

            tableDataChangeListeners.stream().forEach((listener) -> {
                listener.onTableDataChange(entryId, data);
            });
        }

        @Override
        public ConverterContext getConverterContextAt(int i, int i1) {
            return null;
        }

        @Override
        public Class<?> getCellClassAt(int i, int i1) {
            return null;
        }
    }

    private SortableTable table;
    private final ArrayList<TableDataChangeListener> tableDataChangeListeners = new ArrayList();
    private JScrollPane scrollPane;

    ScaffoldTable(Xmap xmap) {
        initComponent(xmap.getColNames(), xmap.getData());
    }

    private void initComponent(String[] columnNames, Object[][] data) {
        setLayout(new GridLayout(1, 0));
        ScaffoldTableModel model;
        if (data.length > 0) {
            // add display column
            int size = columnNames.length;
            String[] newColumnNames = new String[size + 1];
            System.arraycopy(columnNames, 0, newColumnNames, 1, size);
            newColumnNames[0] = "Display";

            // add default value for "display" column 
            int row = data.length;
            int col = data[0].length;

            Object[][] newData = new Object[row][col + 1];

            for (int i = 0; i < row; i++) {
                newData[i][0] = true;

                System.arraycopy(data[i], 0, newData[i], 1, col);
            }
            model = new ScaffoldTableModel(newData, newColumnNames);

        } else {
            model = new ScaffoldTableModel(data, columnNames);
        }

        table = new SortableTable(model);
        table.setFillsViewportHeight(true);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        scrollPane = new JScrollPane(table);
        super.add(scrollPane);
    }

    void addTableChangeListener(TableDataChangeListener tableDataChangeListener) {
        tableDataChangeListeners.add(tableDataChangeListener);
    }

    void update(Xmap xmap) {
        remove(scrollPane);
        initComponent(xmap.getColNames(), xmap.getData());
        updateUI();
    }
}
