/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bionano;

import com.bionano.component.GradientButton;
import com.bionano.event.*;
import static com.bionano.ScaffoldConfig.*;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.SwingWorker;

/**
 *
 * @author quanx
 */
public class ScaffoldPane extends JPanel implements ActionListener {

    // GUI Variables declaration - do not modify
    private final JLabel jLabelMode = new JLabel();
    private final JLabel jLabelRefID = new JLabel();
    private final JLabel jLabelGroupID = new JLabel();
    private final JLabel jLabelConfidence = new JLabel();
    private final JLabel jLabelFontSize = new JLabel();
    private final JLabel jLabelSELineWidth = new JLabel();
    private final JLabel jLabelQryQryH = new JLabel();
    private final JLabel jLabelQryQryV = new JLabel();
    private final JLabel jLabelRefQry = new JLabel();
    private final JLabel jLabelRefRef = new JLabel();

    private final JComboBox<String> jComboBoxMode = new JComboBox<>();
    private final JComboBox<String> jComboBoxRefID = new JComboBox<>();
    private final JComboBox<String> sComboBoxGroupID = new JComboBox();
    private final JComboBox<String> jComboBoxConfidence = new JComboBox<>();
    private final JComboBox<String> jComboBoxFontSize = new JComboBox<>();
    private final JComboBox<String> jComboBoxSELineWidth = new JComboBox<>();
    private final JComboBox<String> jComboBoxQryQryH = new JComboBox<>();
    private final JComboBox<String> jComboBoxQryQryV = new JComboBox<>();
    private final JComboBox<String> jComboBoxRefQry = new JComboBox<>();
    private final JComboBox<String> jComboBoxRefRef = new JComboBox<>();

    private final GradientButton jButtonRulerText = new GradientButton("Ruler-Text", rulerText);
    private final GradientButton jButtonID = new GradientButton("Map-ID", mapID);
    private final GradientButton jButtonMatches = new GradientButton("Matches", matches);
    private final GradientButton jButtonSmoothLine = new GradientButton("Smooth-Lines", smoothLine);

    private final ScaffoldTabbedTablePane tablePanel = new ScaffoldTabbedTablePane(TABLE_PANEL_WIDTH, TABLE_PANEL_HEIGHT);
    private final JToolBar jToolBarMenu = new JToolBar();
    private final JToolBar jToolBarCheckBox = new JToolBar();
    private final JToolBar jToolBarButton = new JToolBar();

    private final ScaffoldSplitPane splitPane = new ScaffoldSplitPane(new SP_SplitPaneListener());

    // End of GUi variables declaration    
    SwingWorker<Void, Void> canvasWorker = new ScaffoldCanvasWorker();
    Cmap rCmap, qCmap;
    Xmap xmap, reversedXmap;
    XmapMode currentMode = XmapMode.INVALID;
    String currentGID;
    int currentRefID;

    File rcmapFile, qcmapFile, xmapFile;

    class SP_SplitPaneListener implements SplitPaneListener {

        @Override
        public void onSplitPaneClick(int id, double confidence) {
            tablePanel.setSecondTable(reversedXmap.getByID(id, confidence), "Scaffold");
            tablePanel.setSelectedIndex(1);
        }
    }

    class ScaffoldPaneTableSetListener implements TableSetListener {

        @Override
        public void onFirstTableSet(int entryId, boolean data) {
            updateUI(true, entryId, data);
        }

        @Override
        public void onSecondTableSet(int entryId, boolean data) {
            updateUI(false, entryId, data);
        }
    }

    public ScaffoldPane() {
        initComponents();
        tablePanel.addTableSetListener(new ScaffoldPaneTableSetListener());
    }

    public void initialize(File rcmapFile, File qcmapFile, File xmapFile) {
        this.rcmapFile = rcmapFile;
        this.qcmapFile = qcmapFile;
        this.xmapFile = xmapFile;
        canvasWorker.execute();
    }

    private void addToolBar(JToolBar toolBar, JLabel jLabel, String name, JComboBox comboBox, Object[] list, String defaultValue) {
        jLabel.setText(name);
        if (list != null) {
            comboBox.setModel(new DefaultComboBoxModel<>(list));
        }
        comboBox.setSelectedItem(defaultValue);
        comboBox.addActionListener(this);
        toolBar.add(jLabel);
        toolBar.add(comboBox);
    }

    private void initToolBarCheckBox() {
        String[] list = new String[]{"0", "5", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55", "60"};
        addToolBar(jToolBarCheckBox, jLabelConfidence, "Confidence", jComboBoxConfidence, list, String.valueOf(ScaffoldConfig.confidence));

        list = new String[]{"3", "5", "7", "9", "11", "13", "15", "17", "19", "21", "23", "25"};
        addToolBar(jToolBarCheckBox, jLabelFontSize, "Font-size", jComboBoxFontSize, list, String.valueOf(ScaffoldConfig.getFontSize()));

        list = new String[]{"0", "1", "5", "10", "15", "20", "25", "30"};
        addToolBar(jToolBarCheckBox, jLabelSELineWidth, "Start-End Width", jComboBoxSELineWidth, list, String.valueOf(ScaffoldConfig.StartEndLineWidth));

        list = new String[]{"0", "1", "5", "10", "15", "20", "25", "30"};
        addToolBar(jToolBarCheckBox, jLabelQryQryH, "Qry-Qry-H", jComboBoxQryQryH, list, String.valueOf(ScaffoldConfig.gapHorizontal));

        list = new String[]{"0", "1", "5", "10", "15", "20", "25", "30"};
        addToolBar(jToolBarCheckBox, jLabelQryQryV, "Qry-Qry-V", jComboBoxQryQryV, list, String.valueOf(ScaffoldConfig.gapVertical));

        list = new String[]{"0", "10", "50", "75", "100", "125", "150", "300"};
        addToolBar(jToolBarCheckBox, jLabelRefQry, "Ref-Qry", jComboBoxRefQry, list, String.valueOf(ScaffoldConfig.gapRefQue));

        list = new String[]{"0", "1", "5", "10", "15", "20", "25", "30"};
        addToolBar(jToolBarCheckBox, jLabelRefRef, "Ref-Ref", jComboBoxRefRef, list, String.valueOf(ScaffoldConfig.gapRefRefHorizontal));

        jToolBarCheckBox.setLayout(new FlowLayout(FlowLayout.LEADING));
        jToolBarCheckBox.setRollover(false);
        jToolBarCheckBox.setFloatable(false);
    }

    private void initToolBarMenu() {
        addToolBar(jToolBarMenu, jLabelMode, "Mode", jComboBoxMode, null, null);
        addToolBar(jToolBarMenu, jLabelGroupID, "Group-ID", sComboBoxGroupID, null, "1");
        addToolBar(jToolBarMenu, jLabelRefID, "Ref-ID", jComboBoxRefID, null, "1");

        jToolBarMenu.setLayout(new FlowLayout(FlowLayout.LEADING));
        jToolBarMenu.setRollover(false);
        jToolBarMenu.setFloatable(false);
    }

    private void initLayout() {
        GroupLayout layout = new GroupLayout(this);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(jToolBarMenu, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE)
                .addComponent(jToolBarCheckBox, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE)
                .addComponent(jToolBarButton, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE)
                .addComponent(splitPane, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE)
                .addComponent(tablePanel, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE)
        );
        layout.setVerticalGroup(layout.createSequentialGroup()
                .addComponent(jToolBarMenu, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(jToolBarCheckBox, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(jToolBarButton, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(splitPane, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE)
                .addComponent(tablePanel, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        );
        setLayout(layout);
    }

    private void initToolBarButton() {
        jToolBarButton.setLayout(new FlowLayout(FlowLayout.LEADING));
        jToolBarButton.setRollover(false);
        jToolBarButton.setFloatable(false);

        jButtonRulerText.addActionListener(this);
        jButtonID.addActionListener(this);
        jButtonMatches.addActionListener(this);
        jButtonSmoothLine.addActionListener(this);

        jToolBarButton.add(jButtonRulerText);
        jToolBarButton.add(jButtonID);
        jToolBarButton.add(jButtonMatches);
        jToolBarButton.add(jButtonSmoothLine);
    }

    private void initComponents() {
        initToolBarMenu();
        initToolBarCheckBox();
        initToolBarButton();
        initLayout();
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        Object source = event.getSource();

        if (source == jComboBoxMode) {
            setMode(getMode((String) jComboBoxMode.getSelectedItem()));
            return;
        }

        confidence = Integer.valueOf((String) jComboBoxConfidence.getSelectedItem());
        setFontSize(Integer.valueOf((String) jComboBoxFontSize.getSelectedItem()));
        StartEndLineWidth = Integer.valueOf((String) jComboBoxSELineWidth.getSelectedItem());
        gapHorizontal = Integer.valueOf((String) jComboBoxQryQryH.getSelectedItem());
        gapVertical = Integer.valueOf((String) jComboBoxQryQryV.getSelectedItem());
        gapRefQue = Integer.valueOf((String) jComboBoxRefQry.getSelectedItem());
        gapRefRefHorizontal = Integer.valueOf((String) jComboBoxRefRef.getSelectedItem());

        if (source == jButtonRulerText) {
            rulerText = !rulerText;
            jButtonRulerText.repaint(rulerText);
        } else if (source == jButtonID) {
            mapID = !mapID;
            jButtonID.repaint(mapID);
        } else if (source == jButtonMatches) {
            matches = !matches;
            jButtonMatches.repaint(matches);
        } else if (source == jButtonSmoothLine) {
            smoothLine = !smoothLine;
            jButtonSmoothLine.repaint(smoothLine);
        }

        switch (currentMode) {
            case BNG:
                String rID = (String) jComboBoxRefID.getSelectedItem();
                if (rID == null) {
                    return;
                }
                int id = Integer.valueOf(rID);
                currentRefID = id;
                splitPane.updateMainCanvas(id);
                tablePanel.setFirstTable(xmap.getByID(currentRefID, confidence), "BNG");
                break;
            case SUPER_CONTIG:
                String groupID = (String) sComboBoxGroupID.getSelectedItem();
                if (groupID == null) {
                    return;
                }
                currentGID = groupID;
                splitPane.updateMainCanvasByGroup(groupID);
                tablePanel.setFirstTable(xmap.getByGroupID(currentGID, confidence), "Group");
                break;
        }
        splitPane.updateMainCanvas();
        splitPane.updateViceCanvas();
    }

    void updateUI(boolean isTopSplitPane, int entryId, boolean display) {
        if (isTopSplitPane) {
            splitPane.updateMainCanvas(entryId, display);
        } else {
            splitPane.updateViceCanvas(entryId, display);
        }
    }

    private void setMode(XmapMode mode) {
        if (mode == currentMode) {
            return;
        }

        switch (mode) {
            case BNG:
                currentMode = XmapMode.BNG;
                jLabelRefID.setVisible(true);
                jComboBoxRefID.setVisible(true);
                jLabelGroupID.setVisible(false);
                sComboBoxGroupID.setVisible(false);
                splitPane.setMainCanvas(rCmap, qCmap, xmap, currentRefID);
                tablePanel.setFirstTable(xmap.getByID(currentRefID, confidence), "BNG");
                setConfidence(10);
                break;

            case SUPER_CONTIG:
                currentMode = XmapMode.SUPER_CONTIG;
                jLabelGroupID.setVisible(true);
                sComboBoxGroupID.setVisible(true);
                jLabelRefID.setVisible(false);
                jComboBoxRefID.setVisible(false);
                splitPane.setMainCanvasByGroupID(rCmap, qCmap, xmap, currentGID);
                tablePanel.setFirstTable(xmap.getByGroupID(currentGID, confidence), "Group");
                setConfidence(0);
                break;

            case CHROMOSOME:
                currentMode = XmapMode.CHROMOSOME;
                break;
        }
        splitPane.setViceCanvas(qCmap.getClone(), rCmap.getClone(), reversedXmap, 1);

    }

    private XmapMode getMode(String modeName) {
        if (modeName.equals(CHROMOSOME)) {
            return XmapMode.CHROMOSOME;
        }
        if (modeName.equals(GROUP)) {
            return XmapMode.SUPER_CONTIG;
        }
        if (modeName.equals(BNG)) {
            return XmapMode.BNG;
        }
        return XmapMode.INVALID;
    }

    private void setConfidence(int conf) {
        confidence = conf;
        jComboBoxConfidence.setSelectedItem("" + conf);
    }

    class ScaffoldCanvasWorker extends SwingWorker<Void, Void> {

        @Override
        public Void doInBackground() {
            rCmap = new Cmap(rcmapFile);
            qCmap = new Cmap(qcmapFile);
            xmap = new Xmap(xmapFile);
            reversedXmap = xmap.getReversed();
            return null;
        }

        @Override
        public void done() {
            setModeComboBox();
            setRefIdComboBox();
            setGroupIDComboBox();
            setMode(xmap.mode);
        }

        private void setRefIdComboBox() {
            int size = rCmap.getSize();
            String[] list = new String[size];
            for (int i = 0; i < size; i++) {
                list[i] = String.valueOf(i + 1);
            }
            jComboBoxRefID.setModel(new DefaultComboBoxModel<>(list));
            currentRefID = rCmap.getFirstID();
        }

        private void setModeComboBox() {
            ArrayList<String> list = new ArrayList();
            XmapMode mode = xmap.mode;
            switch (mode) {
                case CHROMOSOME:
                    list.add(CHROMOSOME);
                case SUPER_CONTIG:
                    list.add(GROUP);
                case BNG:
                    list.add(BNG);
            }
            jComboBoxMode.setModel(new DefaultComboBoxModel<>(list.stream().toArray(String[]::new)));
        }

        private void setGroupIDComboBox() {
            XmapMode mode = xmap.mode;
            if (mode == XmapMode.SUPER_CONTIG || mode == XmapMode.CHROMOSOME) {
                sComboBoxGroupID.setModel(new DefaultComboBoxModel<>(xmap.getGroupNames()));
                currentGID = xmap.getFirstGroupID();
            }
        }

    }
}
